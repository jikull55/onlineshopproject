<?php
 include "session_start.php";
 include "inc_TitlePage.php";
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script type="text/javascript" src="images/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="slimbox/js/slimbox2.js"></script>
<link rel="stylesheet" href="slimbox/css/slimbox2.css" type="text/css" media="screen" />

<link rel="stylesheet" type="text/css" href="css_style_index.css" />
<link rel="stylesheet" type="text/css" href="css_style_menu.css" />

		<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />	
		<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.10.offset.datepicker.min.js"></script>
		<script type="text/javascript">
		  $(function () {
		    var d = new Date();
		    var toDay = d.getDate() + '/' + (d.getMonth() + 1) + '/' + (d.getFullYear() + 543);


		    // กรณีต้องการใส่ปฏิทินลงไปมากกว่า 1 อันต่อหน้า ก็ให้มาเพิ่ม Code ที่บรรทัดด้านล่างด้วยครับ (1 ชุด = 1 ปฏิทิน)

		    $("#datepicker-start").datepicker({ dateFormat: 'dd/mm/yy', isBuddhist: true, defaultDate: toDay, minDate: '-12M', maxDate: '1M', dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
              dayNamesMin: ['อา.','จ.','อ.','พ.','พฤ.','ศ.','ส.'],
              monthNames: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],
              monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.']});
			  
			  $("#datepicker-stop").datepicker({ dateFormat: 'dd/mm/yy', isBuddhist: true, defaultDate: toDay, minDate: '-11M', maxDate: 0, dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
              dayNamesMin: ['อา.','จ.','อ.','พ.','พฤ.','ศ.','ส.'],
              monthNames: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],
              monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.']});
			  

     		 $("#datepicker-en").datepicker({ dateFormat: 'dd/mm/yy'});
		     $("#inline").datepicker({ dateFormat: 'dd/mm/yy', inline: true });


			});
		</script>
		<style type="text/css">

			.demoHeaders { margin-top: 2em; }
			#dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
			#dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
			ul#icons {margin: 0; padding: 0;}
			ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
			ul#icons span.ui-icon {float: left; margin: 0 4px;}
			ul.test {list-style:none; line-height:30px;}
		</style>	
<title><?php echo $TitlePage; ?></title>
</head>
<body id="Page0">
<div class="head">
<?php include "inc_header.php"; ?>
</div>
<div>
  <table border="0" align="center" cellpadding="0" cellspacing="0" class="table_main">
    <tr>
      <td align="left" valign="top" class="table_menu_left" id=""><?php include "inc_menu_left.php"; ?>
      </td>
      <td width="750" align="left" valign="top" class="table_body_center">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="500" align="left" valign="top">
		<div class="title">
		  <h2> <img src="images/page_list_32.png" border="0" /> รายงานข้อมูลรายได้ตามช่วงเวลา</h2>
	</div>
	<div style="padding:0px;">
	  <table width="97%" border="0" cellpadding="0" cellspacing="0" style="padding:10px; margin:10px; border: 1px dashed #666;">
		<form action="adm_report.php?pp=<?=$_GET['pp']; ?>" method="post" name="form1" id="form1" onsubmit="return chk_txt();" >
				  <script language="JavaScript" type="text/javascript">
				  	function chk_txt(){
							if(document.form1.txtDate1.value==""){
									alert("เลือกช่วงวันเวลา ด้วยนะ");
									document.form1.txtDate1.focus();

									return false;
							}
							else if(document.form1.txtDate2.value==""){
									alert("เลือกช่วงวันเวลา ด้วยนะ");
									document.form1.txtDate2.focus();
									return false;
							}
							else {
									return true;
							}
						
					}
				  </script>
          <tr>
            <td height="35" colspan="3" align="left" valign="middle">
			<div style="padding: 5px; border-bottom: 1px solid #ddd; margin-bottom: 5px;"><img src="images/calendar_add.png" width="32" height="32" /><strong> เลือกช่วงเวลาที่ต้องการดูรายงาน สามารถเลือกย้อนหลังได้ 1 ปี </strong></div>			</td>
          </tr>
          <tr>
            <td width="22%" height="27" align="right" valign="middle"><strong>ตั้งแต่วันที่ &nbsp;</strong></td>
            <td width="20%" height="27" valign="middle"><input type="text" style="width: 100px;" id="datepicker-start" name="txtDate1" /></td>
            <td width="58%" height="27" valign="middle"><strong>&nbsp;ถึง </strong>
              <input type="text" style="width: 100px;" id="datepicker-stop" name="txtDate2" />
              <input name="btn_room" type="submit" class="button_txt" id="btn_room" value="เรียกดูรายงาน" /></td>
          </tr>
          <tr>
            <td height="27" colspan="3" align="center" valign="middle">
			<div style="padding:10px; padding-left: 85px; text-align:left;">
<?php if($_POST){
 //แยกวัน เดือน ปี ออกจากกัน
 
$date_ary = explode("/", $_POST['txtDate1']);
$day = $date_ary[0];
$month = $date_ary[1];
$year = $date_ary[2]-543;
$txtDate1 =  $year."-".$month."-".$day; // ทำให้เป็นรูปแบบวันเดือนปีที่ใช้งานได้จริง	
 
 
$date_ary = explode("/", $_POST['txtDate2']);
$day = $date_ary[0];
$month = $date_ary[1];
$year = $date_ary[2]-543;
$txtDate2 =  $year."-".$month."-".$day; // ทำให้เป็นรูปแบบวันเดือนปีที่ใช้งานได้จริง	
				
 ?>
				
                <div style="padding:10px;">
				<a href="print_report.php?Date1=<?php echo $txtDate1; ?>&Date2=<?php echo $txtDate2; ?>" target="_blank">
				  	<img src="images/printer.png" border="0" />
					</a>
				<strong> รายงาน</strong> ตั้งแต่วันที่ <samp style="color:red;"> <?php echo fcDate($txtDate1); ?> </samp> ถึง <samp style="color:red;"><?php echo fcDate($txtDate2); ?> </samp> 
				 </div>
				  
				  
 <?php }else{
$date_back = date("Y-m-d");
$date_ary = explode("-", $date_back);
$day = $date_ary[2];
$month = $date_ary[1];
$year = $date_ary[0];
$day1 ="01";
$txtDate1 =  $year."-".$month."-".$day1; // ทำให้เป็นรูปแบบวันเดือนปีที่ใช้งานได้จริง	
$txtDate2 =  $year."-".$month."-".$day; // ทำให้เป็นรูปแบบวันเดือนปีที่ใช้งานได้จริง	

 ?>
                <div style="padding:10px;">
					<a href="print_report.php?Date1=<?php echo $txtDate1; ?>&Date2=<?php echo $txtDate2; ?>" target="_blank"> 
					 	 <img src="images/printer.png" border="0" /></a>
						 <strong> รายงาน</strong> ตั้งแต่วันที่ <samp style="color:red;"> <?php echo fcDate($txtDate1); ?></samp> ถึง <samp style="color:red;"><?php echo fcDate($txtDate2); ?></samp> 
					  </div>
                <?php } ?>
              </div>			</td>
            </tr>
		  </form>
        </table>
		
	    <table width="100%" height="500" border="0" cellpadding="0" cellspacing="0">

          <tr>
            <td align="left" valign="top" bgcolor="#FDFDFD"><div class="box" style="border:0px; padding:0px;">
<?php  
if($_POST){
//ทำให้เป็นจำนวนเต็มเพื่อใช้คนหา ตามรหัส
$q="SELECT * FROM ".$orders."  WHERE (date(ord_date) BETWEEN '".$txtDate1."' AND '".$txtDate2."') AND ord_status='5' ORDER BY ord_id DESC";  
}else{
//เลือกข้อมูลในตารางออกมาแสดงโดยใช้คำสั่ง SELECT 
$q="SELECT * FROM ".$orders."  WHERE (date(ord_date) BETWEEN '".$txtDate1."' AND '".$txtDate2."') AND ord_status='5' ORDER BY ord_id DESC";  
}


$qr=mysqli_query($con,$q);  
$total=mysqli_num_rows($qr);  
$e_page=20; // กำหนด จำนวนรายการที่แสดงในแต่ละหน้า     

if(!isset($_GET['s_page'])){     
    	$_GET['s_page']=0;     
		
		}else{     
    		$chk_page=$_GET['s_page'];       
 			   $_GET['s_page']=$_GET['s_page']*$e_page;     
		}  
			   
	$q.=" LIMIT ".$_GET['s_page'].",$e_page";  
	$qr=mysqli_query($con,$q);
	  
	if(mysqli_num_rows($qr)>=1){     
    $plus_p=($chk_page*$e_page)+mysqli_num_rows($qr);     
		}else{     
    $plus_p=($chk_page*$e_page);         //$plus_p มีค่าอยู่ที่ 100
	}    
	 
$total_p=ceil($total/$e_page);     
$before_p=($chk_page*$e_page)+1;    //$before_p มีค่าอยู่ที่ 50
?>
<?php  //	ถ้าไม่มีข้อมูล
		if($total==0){
			echo "<div style='padding-top: 20px; color: red;'><h1>ไม่มีข้อมูล</h1></div>";
				}else{
		?>

	  <table width="100%" border="0" cellpadding="0" cellspacing="0 "  style="clear:both">
        <tr>
          <td width="14%" align="center" valign="middle" class="txt_top1">รหัสใบสั่งซื้อ</td>
          <td width="25%" align="center" valign="middle" class="txt_top1">ชื่อ-นามสกุล</td>
          <td width="18%" align="center" valign="middle" class="txt_top1">สถานะ</td>
          <td width="24%" align="center" valign="middle" class="txt_top1">วันที่จัดส่งสินค้า</td>
          <td width="19%" align="center" valign="middle" class="txt_top1">ราคา</td>
          </tr>
		
 <?php
	}							
  $no = $before_p; // ใช้ตัวนี้เป็นตัวแสดงลำดับ
  $i=0;

while($rst=mysqli_fetch_array($qr)){  
$i;
$ID = $rst['ord_id'];
$ord_id = $rst['ord_id'];
$code = sprintf("%05d",$rst['ord_id']);
$name = $rst['ord_name'];
$date = $rst['ord_date'];
$Total = $rst['ord_total'];
$status = $rst['ord_status'];
$txtDate = $rst['ord_date'];

?>
		
         <tr class="report">
          <td width="14%" align="center" valign="middle" class="txt_report-1"><a href="adm_order_report.php?ID=<?php echo $ID; ?>"><?php echo $code; ?></a></td>
          <td width="25%" align="left" valign="middle" class="txt_report-1">คุณ <?php echo $name; ?></td>
          <td width="18%" align="center" valign="middle" class="txt_report-1"><?php  include "inc_status_order.php"; ?></td>
          <td width="24%" align="center" valign="middle" class="txt_report-1"><?php echo $txtDate; ?></td>
          <td align="right" valign="middle" class="txt_report-2"><?php echo number_format($Total,2); ?></td>
          </tr>
		  <?php $no++; $i++?>
            <?php
			$total_price = $total_price + $Total;
			 } ?>
			 <?php if($total>0){ ?>
         <tr class="report0">
           <td colspan="4" align="right" valign="middle" class="txt_report-1"><strong>รวมรายได้</strong></td>
           <td align="right" valign="middle" class="txt_report-2"><b style="color:red;"><?php echo number_format($total_price,2); ?></b></td>
         </tr>
		 <?php } ?>
      </table>

</div>

			
			</td>
          </tr>
        </table>
	  </div>
	</td>
      </tr>
    </table>
	  </td>
    </tr>
</table>
<div class="footer">
<?php include "inc_footer.php"; ?>
</div>
</div>
</body>
</html>