<?php
 session_start(); 
 include "inc_TitlePage.php";
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script type="text/javascript" src="images/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="slimbox/js/slimbox2.js"></script>
<link rel="stylesheet" href="slimbox/css/slimbox2.css" type="text/css" media="screen" />

<link rel="stylesheet" type="text/css" href="css_style_index.css" />
<link rel="stylesheet" type="text/css" href="css_style_menu.css" />


<title><?php echo $TitlePage; ?></title><style type="text/css">

</style></head>
<body id="Page0">
<div class="head">
<?php include "inc_header.php"; ?>
</div>
<div>
<table border="0" align="center" cellpadding="0" cellspacing="0" class="table_main">


  <tr>
    <td align="left" valign="top" class="table_menu_left" id="">
	<?php include "inc_menu_left.php"; ?>	</td>
    <td width="750" align="left" valign="top" class="table_body_center">
    <table width="100%" height="500" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td align="left" valign="top"><div class="title">
		 		<h2> <img src="images/label_32.png" border="0" /> วิธีการสั่งซื้อสินค้า </h2>
			</div>
			<div class="box">
			<ul class="list_data">
			<li class="title_detail" id="brt" style="font-size:16px; padding-bottom: 5px;">
			- <b>วิธีการสั่งซื้อสินค้า</b>			</li>
			<li class="data_detail" id="brt">1. สมัครสมาชิกขอรับ Username และ Password จากนั้นลงชื่อเข้าใช้ระบบเพื่อสั่งซื้อสินค้า</li>
			<li class="data_detail" id="brt">2. คลิกเลือกเมนู รายการสินค้า หยิบสินค้าใส่ตะกร้าสินค้า การสั่งซื้อสินค้าสามารถสั่งซื้อได้มากว่า 1 รายการ </li>		
			<li class="data_detail" id="brt">3. คำนวณราคาสินค้าในตะกร้า จากนั้นยืนยันการสั่งซื้อสินค้า </li>		
			<li class="data_detail" id="brt">4. ชำระเงินค่าสินค้าตามรายการที่สั่งซื้อ โดยโอนเงินผ่านธนาคารที่ทางร้านแจ้งไว้ที่หน้าเว็บไซต์</li>		
			<li class="data_detail" id="brt">5. ลงชื่อเข้าใช้ระบบ เพื่อแจ้งชำระเงินตามรายการสั่งซื้อสินค้าไว้ก่อนหน้านี้</li>		
			<li class="data_detail" id="brt">6. รอรับสินค้าภายใน 7 วัน สมารถตรวจสอบเลขพัสดุในประกาศ</li>		
			<li class="data_detail" id="brt">ขอขอบคุณลูกค้าทุกท่านที่มาใช้บริการ</li>				
	    </ul>
			</div>
			</td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<div class="footer">
<?php include "inc_footer.php"; ?>
</div>
</div>
</body>
</html>