<?php
 include "session_start.php";
 include "inc_TitlePage.php";
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script type="text/javascript" src="images/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="slimbox/js/slimbox2.js"></script>
<link rel="stylesheet" href="slimbox/css/slimbox2.css" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="css_style_index.css" />
<link rel="stylesheet" type="text/css" href="css_style_menu.css" /> 
<title><?php echo $TitlePage; ?></title>
</head>
<body id="Page0">
<div class="head">
<?php include "inc_header.php"; ?>
</div>
<div>
<table border="0" align="center" cellpadding="0" cellspacing="0" class="table_main">


  <tr>
    <td align="left" valign="top" class="table_menu_left" id="">
	<?php include "inc_menu_left.php"; ?>	</td>
    <td width="750" align="left" valign="top" class="table_body_center">
	<div class="title">
		  <h2> <img src="images/key_go.png" border="0" /> เปลี่ยนรหัสผ่านใหม่ </h2>
	</div>
	<div class="box">
    <table width="98%" height="25" border="0" align="center" cellpadding="0" cellspacing="0" style="margin:5px; padding:5px; border: 0px dotted #aaa;">
		 <form action="actionSQL.php" method="post" enctype="multipart/form-data" name="form3" onsubmit="return chk_pass();"> 
					<script language="javascript">
				  	function chk_pass(){
							if(document.form3.txt_oldpass.value==""){
									alert("กรุณากรอก รหัสผ่านเดิม ด้วยนะ");
									document.form3.txt_oldpass.focus();
									return false;
							}
								else if(document.form3.txt_newpass.value=="") {
											alert("กรุณากรอก รหัสผ่าน ด้วยนะ");
											document.form3.txt_newpass.focus();
											return false;
								}
								else if(document.form3.txt_repass.value=="") {
											alert("กรุณากรอก รหัสผ่าน ด้วยนะ");
											document.form3.txt_repass.focus();
											return false;
								}
								else if((document.form3.txt_newpass.value)!=(document.form3.txt_repass.value )){
											alert("กรุณากรอก รหัสผ่านให้เหมือนกัน ด้วยนะ");
											return false;		
								}
								else {
									return true;
							}
						
					}
				  </script>
                    <tr>
                      <td width="162" height="30" align="right" valign="middle"><h3>  รหัสผ่านเดิม : </h3></td>
                      <td width="473" height="30" align="left" valign="middle"><input class="frm" name="txt_oldpass" type="password" id="txt_oldpass" style=" width: 200px;" /></td>
                    </tr>
                    <tr>
                      <td height="30" align="right" valign="middle"><h3>รหัสผ่านใหม่ : </h3></td>
                      <td height="30" align="left" valign="middle"><input class="frm" name="txt_newpass" type="password" id="txt_newpass" style=" width: 200px;" /></td>
                    </tr>
                    <tr>
                      <td height="30" align="right" valign="middle"><h3>รหัสผ่านใหมอีกครั้ง : </h3></td>
                      <td height="30" align="left" valign="middle">
					  <input class="frm" name="txt_repass" type="password" id="txt_repass" style=" width: 200px; margin-left:2px;" /> 
					  <input class="button_txt"  type="submit" name="confirm" id="confirm" value="เปลี่ยนรหัสผ่าน" />
              		  	<input class="button_txt"  type="button" name="button"  id="b" value="ย้อนกลับ" onclick="(history.back())" />
                        <input type="hidden" name="TbName" value="member" />
						<input type="hidden" name="sql" value="REPASS" />	
	
					  </td>
                    </tr>
					</form>
            </table>
   </div>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    </td>
  </tr>
</table>
<div class="footer">
<?php include "inc_footer.php"; ?>
</div>
</div>
</body>
</html>