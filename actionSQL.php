<?php
session_start(); 
error_reporting(0);
error_reporting(E_ERROR | E_PARSE);
 
?>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>

<?php

$TbName = $_REQUEST['TbName'];
if(!empty($TbName)){
// ติดต่อฐานข้อมูล โดยใช้คำสั่ง include 
include "connect.php";
// วันเดือนปี เวลาปัจจุบัน
$DateTime = date("Y-m-d H:i:s", mktime(date("H")+0, date("i")+0, date("s")+0, date("m")+0 , date("d")+0, date("Y")+0));

//***** ----  เริ่มตารางใหม่ ---- ****************************************************************************************************************** member //
if($TbName=="member"){
 //######### ส่วนของการ เพิ่มข้อมูล ลงในตาราง member ############## #########################//
	if($_REQUEST['sql']=="ADD"){  
				$_SESSION['txt_user'] = $_POST['txt_user'];
				$_SESSION['txt_email'] = $_POST['txt_email'];
				$_SESSION['txt_name'] = $_POST['txt_name'];
				$_SESSION['txt_date'] = $_POST['txt_date'];
				$_SESSION['txt_tel'] = $_POST['txt_tel'];
				$_SESSION['txt_ads1'] = $_POST['txt_ads'][0];
				$_SESSION['txt_ads2'] = $_POST['txt_ads'][1];
				$_SESSION['txt_ads3'] = $_POST['txt_ads'][2];
				$_SESSION['txt_ads4'] = $_POST['txt_ads'][3];
				$_SESSION['txt_ads5'] = $_POST['txt_ads'][4];	
				
$Address = "".$_POST['txt_ads'][0]."-".$_POST['txt_ads'][1]."-".$_POST['txt_ads'][2]."-".$_POST['txt_ads'][3]."-".$_POST['txt_ads'][4]."";

if(!is_numeric($_POST['txt_ads'][4])) { 
			exit("<script>alert('กรุณากรอก รหัสไปรษณีย์ เป็นตัวเลขเท่านั้น !');(history.back());</script>");
			}		
if(strlen($_POST['txt_user'])<4){
			exit("<script>alert('ชื่อเข้าใช้ระบบไม่ควรน้อยกว่า 4 ตัวอักษร !');(history.back());</script>");
			}
			
if (!filter_var($_POST['txt_email'], FILTER_VALIDATE_EMAIL)) {	
				exit("<script>alert('รูปแบบ e-mail ไม่ถูกต้อง');(history.back());</script>");
			}
if(strlen($_POST['txt_pass'])<4){
			exit("<script>alert('รหัสผ่านไม่ควรน้อยกว่า 4 ตัวอักษร !');(history.back());</script>");
			}
if(!is_numeric($_POST['txt_tel'])) { 
			exit("<script>alert('กรุณากรอก - เบอร์โทร เป็นตัวเลขเท่านั้น !');(history.back());</script>");
			}		
if(strlen($_POST['txt_tel'])<10){
			exit("<script>alert('กรุณากรอกเบอร์โทรให้ครับ 10 ตัวด้วยนะ !');(history.back());</script>");
			}
if(empty($_POST['txt_date'])){
			exit("<script>alert('รูปแบบวันที่ไม่ถูกต้อง !');(history.back());</script>");
			}else{
				$date_ary = explode("/", $_POST['txt_date']);
				$day = $date_ary[0];
				$month = $date_ary[1];
				$year = $date_ary[2];
				$birthday =  $year."-".$month."-".$day; // ทำให้เป็นรูปแบบวันเดือนปีที่ใช้งานได้จริง	
			}	
//ดึงข้อมูลในตาราง มาตรวจว่ามีข้อมูล่ซ่ำกันหรือไม่
$sql = mysqli_query($con,"SELECT * FROM ".$member." WHERE mb_user='".$_POST['txt_user']."'");
		$num = mysqli_num_rows($sql);
		$rst = mysqli_fetch_array($sql);
		//ตรวจสอบด้วยเงื้อไข if 
		if($num >= 1){
				$name = $rst['mb_user'];
				exit("<script>alert('ชื่อเข้าใช้ระบบ [ ".$name." ] ถูกใช้งานแล้ว');(history.back());</script>"); 
				}
		
$status  = "อนุญาตให้ใช้ระบบ"; // สถานะให้เข้าระบบได้
$TxtAddress = trim($Address);		
//เพิ่มข้อมูลลงในตารางโดยส่งค่ามาจาก ฟอร์ม
$sql_insert = mysqli_query($con,"INSERT INTO ".$member." VALUES "
		."(0,
		'".$_POST['txt_user']."',
		'".$_POST['txt_pass']."',
		'".$_POST['txt_name']."',
		'".$birthday."',
		'".$TxtAddress."',
		'".$_POST['txt_tel']."',
		'".$_POST['txt_email']."',
		'".$status."',
		'".$DateTime."')");
		
	if($sql_insert){
		$last_id = mysqli_insert_id($con);
		// ลบ Session		
		
		unset($_SESSION['txt_user']);
		unset($_SESSION['txt_email']);
		unset($_SESSION['txt_name']);
		unset($_SESSION['txt_date']);
		unset($_SESSION['txt_tel']);
		unset($_SESSION['txt_ads1']);
		unset($_SESSION['txt_ads2']);
		unset($_SESSION['txt_ads3']);
		unset($_SESSION['txt_ads4']);
		unset($_SESSION['txt_name']);
		unset($_SESSION['txt_ads5']);
 
	exit("<script>alert('บันทึกข้อมูลการสมัครสมาชิกเรียบร้อยแล้ว');window.location='index.php';</script>");	
	}else{
	exit("<script>alert('สมัครสมาชิกไม่ได้! ');(history.back());</script>");
	}

 //######### ส่วนของการ แก้ไขข้อมูล member #######################################//
}else if($_REQUEST['sql']=="UPDATE"){ 

	if(!is_numeric($_POST['txt_tel'])) { 
				exit("<script>alert('กรุณากรอก - เบอร์โทร เป็นตัวเลขเท่านั้น !');(history.back());</script>");
				}		
	if(strlen($_POST['txt_tel'])<10){
				exit("<script>alert('กรุณากรอกเบอร์โทรให้ครับ 10 ตัวด้วยนะ !');(history.back());</script>");
				}
	if(!ereg("([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})",$_POST['txt_date'])){
				exit("<script>alert('รูปแบบวันที่ไม่ถูกต้อง !');(history.back());</script>");
				}else{
					$date_ary = explode("/", $_POST['txt_date']);
					$day = $date_ary[0];
					$month = $date_ary[1];
					$year = $date_ary[2];
					$birthday =  $year."-".$month."-".$day; // ทำให้เป็นรูปแบบวันเดือนปีที่ใช้งานได้จริง	
					}	
	$Address = "".$_POST['txt_ads'][0]."-".$_POST['txt_ads'][1]."-".$_POST['txt_ads'][2]."-".$_POST['txt_ads'][3]."-".$_POST['txt_ads'][4]."";
	$TxtAddress = trim($Address);		
	$sql_update = mysqli_query($con,"UPDATE ".$member." SET "
							
							." mb_name ='".$_POST['txt_name']."', 
								mb_birthday ='".$birthday."', 
								mb_address ='".$TxtAddress."', 
								mb_tel ='".$_POST['txt_tel']."', 
								mb_email ='".$_POST['txt_email']."', 
								mb_status='".$_POST['txt_status']."' WHERE mb_id='".$_POST['ID']."'");
if($sql_update){
	exit("<script>alert('บันทึกข้อมูลแล้ว ');(history.back());</script>");
	}else{
	exit("<script>alert('บันทึกข้อมูลไม่ได้!');(history.back());</script>");
	}		
 //######### ส่วนของการ เปลี่ยนรหัสผ่านใหม่ member #######################################//
}else if($_REQUEST['sql']=="REPASS"){  

if(strlen($_POST['txt_newpass'])<4){
			exit("<script>alert('รหัสผ่านไม่ควรน้อยกว่า 4 ตัวอักษร !');(history.back());</script>");
			}

	$sql_Repassword = mysqli_query($con,"SELECT * FROM ".$member." WHERE mb_pass='".$_POST['txt_oldpass']."' AND mb_id='".$_SESSION['sess_mb_id']."'");
	$num = mysqli_num_rows($sql_Repassword);
		if($num==1){
			$sql_update = mysqli_query($con,"UPDATE ".$member." SET mb_pass='".$_POST['txt_newpass']."' WHERE mb_id='".$_SESSION['sess_mb_id']."'");
			if($sql_update){
					exit("<script>alert('ระบบได้เปลี่ยนรหัสผ่านใหม่แล้ว');window.location='mb_profile.php';</script>");	
						}else{
						exit("<script>alert('error : ไม่สามารถเปลี่ยนรหัสผ่านได้!');(history.back());</script>");
						}
			}else{
				exit("<script>alert('error : รหัสผ่านไม่ถูกต้อง!');(history.back());</script>");
				}
				
 //######### ส่วนของการ ลบ member #######################################//
}else if($_REQUEST['sql']=="DEL"){  
	// ลบข้อมูลตามรหัสที่ส่งมา 
	$sql_delete = mysqli_query($con,"DELETE FROM ".$member." WHERE mb_id='".$_REQUEST['ID']."'");
	if($sql_delete){
	exit("<script>window.location='adm_member.php?pp=1';</script>");	
	}else{
	exit("<script>alert('error : ลบข้อมูลไม่ได้!');(history.back());</script>");
	}		
	
}else{
		exit("<script>alert('error : ค้นหา SQL ตาราง ".$TbName." ไม่เจอ!');(history.back());</script>");
		} // End SQL
	
	
//***** ----  เริ่มตารางใหม่ ---- ****************************************************************************************************************** admin //
}else if($TbName=="admin"){ 

	//######### ส่วนของการ แก้ไขข้อมูลส่วนตัว admin #######################################//
	if($_REQUEST['sql']=="UPDATE"){ 
	
		if(!is_numeric($_POST['txt_tel'])) { 
					exit("<script>alert('กรุณากรอก - เบอร์โทร เป็นตัวเลขเท่านั้น !');(history.back());</script>");
					}		
		if(strlen($_POST['txt_tel'])<10){
					exit("<script>alert('กรุณากรอกเบอร์โทรให้ครับ 10 ตัวด้วยนะ !');(history.back());</script>");
					}
		if(!ereg("([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})",$_POST['txt_date'])){
					exit("<script>alert('รูปแบบวันที่ไม่ถูกต้อง !');(history.back());</script>");
					}else{
						$date_ary = explode("/", $_POST['txt_date']);
						$day = $date_ary[0];
						$month = $date_ary[1];
						$year = $date_ary[2];
						$birthday =  $year."-".$month."-".$day; // ทำให้เป็นรูปแบบวันเดือนปีที่ใช้งานได้จริง	
						}	
		$Address = "".$_POST['txt_ads'][0]."-".$_POST['txt_ads'][1]."-".$_POST['txt_ads'][2]."-".$_POST['txt_ads'][3]."-".$_POST['txt_ads'][4]."";
		$TxtAddress = trim($Address);		
		$sql_update = mysqli_query($con,"UPDATE ".$admin." SET "
								
								." adm_name ='".$_POST['txt_name']."', 
									adm_birthday ='".$birthday."', 
									adm_address ='".$TxtAddress."', 
									adm_tel ='".$_POST['txt_tel']."', 
									adm_email ='".$_POST['txt_email']."' WHERE adm_id='".$_POST['ID']."'");
	if($sql_update){
		exit("<script>alert('บันทึกข้อมูลแล้ว ');(history.back());</script>");
		}else{
		exit("<script>alert('บันทึกข้อมูลไม่ได้!');(history.back());</script>");
		}		

 //######### ส่วนของการ เปลี่ยนรหัสผ่านใหม่ admin #######################################//
}else if($_REQUEST['sql']=="REPASS"){  

if(strlen($_POST['txt_newpass'])<4){
			exit("<script>alert('รหัสผ่านไม่ควรน้อยกว่า 4 ตัวอักษร !');(history.back());</script>");
			}

	$sql_Repassword = mysqli_query($con,"SELECT * FROM ".$admin." WHERE adm_pass='".$_POST['txt_oldpass']."' AND adm_id='".$_SESSION['sess_adm_id']."'");
	$num = mysqli_num_rows($sql_Repassword);
		if($num==1){
			$sql_update = mysqli_query($con,"UPDATE ".$admin." SET adm_pass='".$_POST['txt_newpass']."' WHERE adm_id='".$_SESSION['sess_adm_id']."'");
			if($sql_update){
					exit("<script>alert('ระบบได้เปลี่ยนรหัสผ่านใหม่แล้ว');window.location='adm_profile.php?pp=1';</script>");	
						}else{
						exit("<script>alert('error : ไม่สามารถเปลี่ยนรหัสผ่านได้!');(history.back());</script>");
						}
				}else{
				exit("<script>alert('error : รหัสผ่านไม่ถูกต้อง!');(history.back());</script>");
				}
	}else{
		exit("<script>alert('error : ค้นหา SQL ตาราง ".$TbName." ไม่เจอ!');(history.back());</script>");
		} // End SQL
	
//***** ----  เริ่มตารางใหม่ ---- ****************************************************************************************************************** product_type //
}else if($TbName=="product_type"){ 

 //#########  product_type #######################################//
	if($_REQUEST['sql']=="ADD"){  
//ดึงข้อมูลในตาราง มาตรวจว่ามีข้อมูล่ซ่ำกันหรือไม่
$sql = mysqli_query($con,"SELECT * FROM ".$product_type." WHERE type_name='".$_POST['txt_name']."'");
		$num = mysqli_num_rows($sql);
		$rst = mysqli_fetch_array($sql);
		//ตรวจสอบด้วยเงื้อไข if 
		if($num >= 1){
				$name = $rst['type_name'];
				exit("<script>alert('ข้อมูล [ ".$name." ] มีในระบบแล้ว');(history.back());</script>"); 
				}
	
		//เพิ่มข้อมูลลงในตารางโดยส่งค่ามาจาก ฟอร์ม
		$sql_insert = mysqli_query($con,"INSERT INTO ".$product_type." VALUES "
				."(0,
				'".$_POST['txt_name']."')");
				
		if($sql_insert){
			exit("<script>alert('บันทึกข้อมูลเรียบร้อยแล้ว');window.location='adm_product_type.php?pp=2';</script>");	
			}else{
			exit("<script>alert('error : บันทึกข้อมูลไม่ได้! ');(history.back());</script>");
			}
 //#########  product_type #######################################//
	}else if($_REQUEST['sql']=="UPDATE"){ 
		$sql_update = mysqli_query($con,"UPDATE ".$product_type." SET type_name ='".$_POST['txt_name']."' WHERE type_id='".$_POST['ID']."'");
		
		if($sql_update){
		exit("<script>alert('บันทึกข้อมูลแล้ว ');(history.back());</script>");
		}else{
		exit("<script>alert('บันทึกข้อมูลไม่ได้!');(history.back());</script>");
		}	
 //#########  product_type #######################################//
	}else if($_REQUEST['sql']=="DEL"){ 
			// ลบข้อมูลตามรหัสที่ส่งมา 
		$sql_delete = mysqli_query($con,"DELETE FROM ".$product_type." WHERE type_id='".$_REQUEST['ID']."'");
		if($sql_delete){
		exit("<script>window.location='adm_product_type.php?pp=2';</script>");	
		}else{
		exit("<script>alert('error : ลบข้อมูลไม่ได้!');(history.back());</script>");
		}		
		
}else{
		exit("<script>alert('error : ค้นหา SQL ตาราง ".$TbName." ไม่เจอ!');(history.back());</script>");
		} // End SQL

//***** ----  เริ่มตารางใหม่ ---- ****************************************************************************************************************** product //
}else if($TbName=="product"){ 
//#########  product #######################################//

	if($_REQUEST['sql']=="ADD"){  
	
	//ดึงข้อมูลในตาราง มาตรวจว่ามีข้อมูล่ซ่ำกันหรือไม่
	$sql_chk = mysqli_query($con,"SELECT * FROM ".$product." WHERE prd_name='".$_POST['txt_name']."' AND prd_type_id='".$_POST['txt_type']."'");
			$check = mysqli_num_rows($sql_chk);
			$result = mysqli_fetch_array($sql_chk);

	//ตรวจสอบด้วยเงื้อไข if 
	if($check >= 1){
		$check_name = $result['prd_name'];
		exit("<script>alert('ข้อมูล [ ".$check_name." ]  มีในอยู่ระบบแล้ว');(history.back());</script>"); 
		exit();
		}

	$FileName 	= $_FILES['FileUpload'] ['name'];
	$Filetype 		= $_FILES['FileUpload'] ['type'];
	$FileSize 		= $_FILES['FileUpload'] ['size'];
	$FileUpLoadtmp = $_FILES['FileUpload'] ['tmp_name'];
			
if($FileUpLoadtmp){					 
	$array_last = explode(".",$FileName); // เป็น array หาจำนวน จุด . ของชื่อตัวแปร์		
	$c = count($array_last) - 1; //นับจำนวน จุด "." ของชื่อตัวแปร์ 
	$lname = strtolower($array_last [$c]); // หา นามสกุลไฟล์ ตัวสุดท้ายของ ตัวแปร์
	$NewFileupload = date("U"); 
	$NewFile = $NewFileupload.".$lname"; //รวม ชื่อและนามสกลุดไฟล์เข้าด้วยกัน 
	}
	
$txtDetail = eregi_replace(chr(13), "<br>", $_POST['txt_detail']);
$TxtSell = "";
//เพิ่มข้อมูลลงในตารางโดยส่งค่ามาจาก ฟอร์ม
$sql_insert = mysqli_query($con,"INSERT INTO ".$product." VALUES "
		."(0,
		'".$_POST['txt_type']."', 
		'".$_POST['txt_name']."', 
		'".$_POST['txt_price']."', 
		'".$_POST['txt_size']."', 
		'".$txtDetail."', 
		'".$_POST['txt_stock']."', 
		'".$TxtSell."',
		'".$NewFile."',
		'".$_POST['txt_status']."')");
		
if($sql_insert){
$last_id = mysqli_insert_id($con);

			if($lname=="gif" or $lname=="jpg" or $lname=="jpeg" or $lname=="png"){
				//Upload File รูปภาพลงในโฟลเดอร์  photo
				$UploadFile = move_uploaded_file($FileUpLoadtmp, "photo/".$NewFile);					
				}	
				
exit("<script>alert('บันทึกข้อมูลแล้ว');window.location='adm_product.php?pp=2';</script>");	
}else{
exit("<script>alert('Error : บันทึกข้อมูลไม่ได้! ');(history.back());</script>");
}

	
 //#########  product #######################################//
	}else if($_REQUEST['sql']=="UPDATE"){ 
	
	// ถ้ามีการส่งค่าแบบ post มาให้ทำตามเงื้อนไขในปีกกา
if($_POST['checkbox']==1){
							@unlink("photo/".$_POST['photo']);
							$Null = "";
							$sql_update = mysqli_query($con,"UPDATE ".$product." SET prd_photo='".$Null."'  WHERE prd_id='".$_REQUEST['ID']."'");
							}
$txtDetail = eregi_replace(chr(13),"<br>",$_POST['txt_detail']);						
$sql_update = mysqli_query($con,"UPDATE ".$product." SET "
							
							."  prd_type_id ='".$_POST['txt_type']."', 
								prd_name ='".$_POST['txt_name']."', 
								prd_size ='".$_POST['txt_size']."', 
								prd_detail ='".$txtDetail."', 
								prd_stock  ='".$_POST['txt_stock']."', 
								prd_price  ='".$_POST['txt_price']."', 
								prd_status  ='".$_POST['txt_status']."' WHERE prd_id='".$_REQUEST['ID']."'");
								
// ส่วนของรูปภาพ
if($_FILES){								
	$FileName 	= $_FILES['FileUpload'] ['name'];
	$Filetype 		= $_FILES['FileUpload'] ['type'];
	$FileSize 		= $_FILES['FileUpload'] ['size'];
	$FileUpLoadtmp = $_FILES['FileUpload'] ['tmp_name'];
			
if($FileUpLoadtmp){					 
	$array_last = explode(".",$FileName); // เป็น array หาจำนวน จุด . ของชื่อตัวแปร์		
	$c = count($array_last) - 1; //นับจำนวน จุด "." ของชื่อตัวแปร์ 
	$lname = strtolower($array_last [$c]); // หา นามสกุลไฟล์ ตัวสุดท้ายของ ตัวแปร์
	$NewFileupload = date("U"); 
	$NewFile = $NewFileupload.".$lname"; //รวม ชื่อและนามสกลุดไฟล์เข้าด้วยกัน 

	if($lname=="gif" or $lname=="jpg" or $lname=="jpeg" or $lname=="png"){
	
								if(move_uploaded_file($FileUpLoadtmp, "photo/".$NewFile)){ 
									 @unlink("photo/".$_POST['photo']);
									$sql_update_photo = mysqli_query($con,"UPDATE ".$product." SET prd_photo='".$NewFile."' WHERE prd_id='".$_REQUEST['ID']."'");
								}else{
									echo "NO sucsess <br />";
					}}}
	
} // จบส่วนของรูปภาพ

if($sql_update){
exit("<script>alert('บันทึกข้อมูลแล้ว ');(history.back());</script>");
}else{
exit("<script>alert('Error : บันทึกข้อมูลไม่ได้! ');(history.back());</script>");
}
	
 //#########  product #######################################//
}else if($_REQUEST['sql']=="DEL"){ 
		$sql = mysqli_query($con,"SELECT * FROM ".$product." WHERE prd_id = '".$_REQUEST['ID']."'");
		$rs = mysqli_fetch_array($sql);
		//ลบรูปสินค้าในโฟลเด้อ Product 
		@unlink("photo/".$rs['prd_photo']);
		
		$sql_delete =mysqli_query($con,"DELETE FROM ".$product." WHERE prd_id='".$_REQUEST['ID']."'");
		if($sql_delete){
		exit("<script>window.location='adm_product.php?pp=2';</script>");	
		}else{
		exit("<script>alert('error : ลบข้อมูลไม่ได้!');(history.back());</script>");
		}	
		
// ---------------------------------------------//
}else{
		exit("<script>alert('error : ค้นหา SQL ตาราง ".$TbName." ไม่เจอ!');(history.back());</script>");
		} // End SQL
	
	
//***** ----  เริ่มตารางใหม่ ---- ****************************************************************************************************************** bank //
}else if($TbName=="bank"){ 
 //#########  bank #######################################//
	if($_REQUEST['sql']=="ADD"){  
	
	//ดึงข้อมูลในตาราง มาตรวจว่ามีข้อมูล่ซ่ำกันหรือไม่
		$sql_chk = mysqli_query($con,"SELECT * FROM ".$bank." WHERE bn_bank='".$_POST['txt_bank']."' AND bn_number='".$_POST['txt_number']."'");
				$check = mysqli_num_rows($sql_chk);
				$result = mysqli_fetch_array($sql_chk);
	
		//ตรวจสอบด้วยเงื้อไข if 
		if($check >= 1){
			$check_name = $result['bn_bank'];
			exit("<script>alert('ข้อมูล [ ".$check_name." ]  มีในอยู่ระบบแล้ว');(history.back());</script>"); 
			exit();
			}
	
		$FileName 	= $_FILES['FileUpload'] ['name'];
		$Filetype 		= $_FILES['FileUpload'] ['type'];
		$FileSize 		= $_FILES['FileUpload'] ['size'];
		$FileUpLoadtmp = $_FILES['FileUpload'] ['tmp_name'];
				
	if($FileUpLoadtmp){					 
		$array_last = explode(".",$FileName); // เป็น array หาจำนวน จุด . ของชื่อตัวแปร์		
		$c = count($array_last) - 1; //นับจำนวน จุด "." ของชื่อตัวแปร์ 
		$lname = strtolower($array_last [$c]); // หา นามสกุลไฟล์ ตัวสุดท้ายของ ตัวแปร์
		$NewFileupload = date("U"); 
		$NewFile = $NewFileupload.".$lname"; //รวม ชื่อและนามสกลุดไฟล์เข้าด้วยกัน 
		}
	
	//เพิ่มข้อมูลลงในตารางโดยส่งค่ามาจาก ฟอร์ม
	$sql_insert = mysqli_query($con,"INSERT INTO ".$bank." VALUES "
			."(0,
			'".$_POST['txt_bank']."', 
			'".$_POST['txt_name']."', 
			'".$_POST['txt_number']."', 
			'".$_POST['txt_branch']."', 
			'".$NewFile."')");
			
	if($sql_insert){
	$last_id = mysqli_insert_id($con);
	
				if($lname=="gif" or $lname=="jpg" or $lname=="jpeg" or $lname=="png"){
					//Upload File รูปภาพลงในโฟลเดอร์  photo
					$UploadFile = move_uploaded_file($FileUpLoadtmp, "photo/".$NewFile);					
					}	
					
	exit("<script>window.location='adm_bank.php?pp=2';</script>");	
	}else{
	exit("<script>alert('Error : บันทึกข้อมูลไม่ได้! ');(history.back());</script>");
	}
		
 //#########  bank #######################################//
	}else if($_REQUEST['sql']=="UPDATE"){ 
	
	// ถ้ามีการส่งค่าแบบ post มาให้ทำตามเงื้อนไขในปีกกา
	if($_POST['checkbox']==1){
								@unlink("photo/".$_POST['photo']);
								$Null = "";
								$sql_update = mysqli_query($con,"UPDATE ".$bank." SET bn_photo='".$Null."'  WHERE bn_id='".$_POST['ID']."'");
								}
								
	$sql_update = mysqli_query($con,"UPDATE ".$bank." SET "
								
								." bn_bank ='".$_POST['txt_bank']."', 
									bn_name ='".$_POST['txt_name']."', 
									bn_number ='".$_POST['txt_number']."', 
									bn_branch ='".$_POST['txt_branch']."' WHERE bn_id='".$_POST['ID']."'");
									
	// ส่วนของรูปภาพ
	if($_FILES){								
		$FileName 	= $_FILES['FileUpload'] ['name'];
		$Filetype 		= $_FILES['FileUpload'] ['type'];
		$FileSize 		= $_FILES['FileUpload'] ['size'];
		$FileUpLoadtmp = $_FILES['FileUpload'] ['tmp_name'];
				
	if($FileUpLoadtmp){					 
		$array_last = explode(".",$FileName); // เป็น array หาจำนวน จุด . ของชื่อตัวแปร์		
		$c = count($array_last) - 1; //นับจำนวน จุด "." ของชื่อตัวแปร์ 
		$lname = strtolower($array_last [$c]); // หา นามสกุลไฟล์ ตัวสุดท้ายของ ตัวแปร์
		$NewFileupload = date("U"); 
		$NewFile = $NewFileupload.".$lname"; //รวม ชื่อและนามสกลุดไฟล์เข้าด้วยกัน 
	
		if($lname=="gif" or $lname=="jpg" or $lname=="jpeg" or $lname=="png"){
		
									if(move_uploaded_file($FileUpLoadtmp, "photo/".$NewFile)){ 
										 @unlink("photo/".$_POST['photo']);
										$sql_update_photo = mysqli_query($con,"UPDATE ".$bank." SET bn_photo='".$NewFile."' WHERE bn_id='".$_POST['ID']."'");
									}else{
										echo "NO sucsess <br />";
						}}}
		
	} // จบส่วนของรูปภาพ
	
	if($sql_update){
	exit("<script>alert('บันทึกข้อมูลแล้ว ');(history.back());</script>");
	}else{
	exit("<script>alert('Error : บันทึกข้อมูลไม่ได้! ');(history.back());</script>");
	}
	
 //#########  bank #######################################//
	}else if($_REQUEST['sql']=="DEL"){ 
	
	$ID = $_GET['ID'];
		
		// ดึงข้อมูลในตาราง
		$sql =mysqli_query($con,"SELECT * FROM ".$bank." WHERE bn_id = '".$ID."'");
		$rst = mysqli_fetch_array($sql);
		//ลบรูปภาพ
		@unlink("photo/".$rst['bn_photo']);
		
		// ลบข้อมูลตามรหัสที่ส่งมา 
		$Del = mysqli_query($con,"DELETE FROM ".$bank."  WHERE bn_id='".$ID."'");
		
		if($Del){
			exit("<script>window.location='adm_bank.php?pp=2&m=1';</script>");	
			}else{
			exit("<script>alert('Error : บันทึกข้อมูลไม่ได้! ');(history.back());</script>");
			}		
	
}else{
		exit("<script>alert('error : ค้นหา SQL ตาราง ".$TbName." ไม่เจอ!');(history.back());</script>");
		} // End SQL
	
//***** ----  เริ่มตารางใหม่ ---- ****************************************************************************************************************** orders //
}else if($TbName=="orders"){ 

	//***** ----  ลงทะเบียน Session ---- ****************************************************************************************************************** SESS_RGT //
	 if($_REQUEST['sql']=="SESS_RGT"){ 
		
		if(empty($_SESSION['sess_mb_id'])){
			exit("<script>alert('กรุณาก Login เข้าระบบก่อนนะค่ะ !');(history.back());</script>");
		}		
				
				
				$ID=$_REQUEST['ID'];
				//ลงทะเบียน session

					if(count($_SESSION['sess_id'])=="0"){ 
						 $check=1;
						//ค้นหาว่ามีข้อมูลรหัสสินค้าอยู่ใน sess_id หรือไม่ ถ้าไม่มีให้ check เท่ากับ 1
						}else if (!in_array($ID, $_SESSION['sess_id'])){
						 $check=1;
						}
					
					if($check==1){
							$sql = mysqli_query($con,"SELECT * FROM ".$product." WHERE prd_id='".$_GET['ID']."'");
							$rs1=mysqli_fetch_array($sql);
							 $_SESSION['sess_id'][]=$rs1['prd_id']; 
							 $_SESSION['sess_num'][]=1;
							}
							exit("<script>window.location='basket_product.php';</script>");	
							
//#########  SESS_DEL #######################################//					
}else if($_REQUEST['sql']=="SESS_DEL"){ 						
				
				$ID=$_REQUEST['ID'];
				for($i=0; $i<count($_SESSION['sess_id']);$i++) {
					// ค้นหา session  ที่ต้องการลบ
					if(!in_array($_SESSION['sess_id'][$i], $ID)){
						$_GET['temp_id'][]=$_SESSION['sess_id'][$i];
						$_GET['temp_num'][]=$_SESSION['sess_num'][$i];			
						}
				} // จบ for
				// ลบ session 
				$_SESSION['sess_id']=$_GET['temp_id'];
				$_SESSION['sess_num']=$_GET['temp_num'];
				exit("<script>window.location='basket_product.php';</script>");	
				
//#########  BKT_Calculate  #######################################//					
}else if($_REQUEST['sql']=="BKT_Calculate"){ 		
			  for($i=0; $i<count($_SESSION['sess_id']);$i++) {
					
					if(!is_numeric($_POST['txt_num'][$i])){ // ตรวจสอบกรอกจำนวนสินค้าให้ถูก
								exit("<script>alert('กรุณากรอกจำนวนสินค้าให้ถูกด้วยนะ!');(history.back());</script>");
								}else if(strpos($_POST['txt_num'][$i],".") !== false){
								exit("<script>alert('กรุณากรอกจำนวนสินค้าให้ถูกด้วยนะ!');(history.back());</script>");
								}
										//ส่วนของการตรวจสอบ สินค้าที่ลูกค้าสั่งเกิน
										$sql = mysqli_query($con,"SELECT * FROM ".$product." WHERE prd_id='".$_SESSION['sess_id'][$i]."'");
											$rst = mysqli_fetch_array($sql);
											$stock = $rst['prd_stock'];
											$name = $rst['prd_name'];
											
										if($stock < $_POST['txt_num'][$i]){
											exit("<script>alert('สินค้า ".$name." ที่คุณสั่งซื้อไม่พอ คุณสามารถสั่งซื้อสินค้าได้ ".$stock."');(history.back());</script>");
											}else if($_POST['txt_num'][$i]<1){
											exit("<script>alert('กรุณากรอกจำนวนให้ถูกต้องด้วยนะ!');(history.back());</script>");
											}else{
											$_POST['temp_num'][]=$_POST['txt_num'][$i];
											}	//ส่วนของการตรวจสอบ stock
											
								} // end for
								
					$_SESSION['sess_num']=$_POST['temp_num'];
					if($_POST['calculate']){
							exit("<script>window.location='basket_product.php';</script>");	
							}else if($_POST['complete']){
							exit("<script>window.location='baskt_product_buy.php';</script>");	
							}else if($_POST['back']){
							exit("<script>window.location='product.php?p=New';</script>");	
							}
							
//#########  BKT_Confirm  #######################################//					
}else if($_REQUEST['sql']=="BKT_Confirm"){ 		
		
		 if($_POST['back']){
					exit("<script>window.location='basket_product.php';</script>");	
					}else if($_POST['confirm']){
						$sql = mysqli_query($con,"SELECT * FROM ".$member." WHERE mb_id='".$_SESSION['sess_mb_id']."'");
						$rst = mysqli_fetch_array($sql);
						
			$status = "1";
			$DateTime = date("Y-m-d H:i:s", mktime(date("H")+0, date("i")+0, date("s")+0, date("m")+0 , date("d")+0, date("Y")+0));
			
	//เพิ่มข้อมูลลงในตารางโดยส่งค่ามาจาก ฟอร์ม
	$sql_insert = mysqli_query($con,"INSERT INTO ".$orders."  VALUES "
			."(0,
			'".$rst['mb_id']."', 
			'".$rst['mb_name']."', 
			'".$rst['mb_tel']."', 
			'".$_POST['txt_Total']."', 
			'".$DateTime."', 
			'".$status."')");		
					
			if($sql_insert){	
					$last_id = mysqli_insert_id($con);	
					for($i=0;$i<count($_SESSION['sess_id']);$i++){
					$sql = mysqli_query($con,"SELECT * FROM ".$product." WHERE prd_id='".$_SESSION['sess_id'][$i]."'");
								$rst=mysqli_fetch_array($sql);
								$price = $rst['prd_price'];
								$stock = $rst['prd_stock'];
								$sell = $rst['prd_sell'];

								
						$sql_insert = mysqli_query($con,"INSERT INTO ".$orders_detail." VALUES ('".$last_id."', 
																																 '".$_SESSION['sess_id'][$i]."', 
																																 '".$_SESSION['sess_num'][$i]."',
																																 '".$price."')");
					$up_stock = $stock - $_SESSION['sess_num'][$i];
					$up_sell = $sell + $_SESSION['sess_num'][$i];																					 
					$sql_stock =mysqli_query($con,"UPDATE ".$product." SET prd_sell='".$up_sell."', prd_stock='".$up_stock."' WHERE prd_id='".$_SESSION['sess_id'][$i]."'");
																																 
																																} // end for
																																
																															//ลบ session
																															unset($_SESSION['sess_id']);
																															unset($_SESSION['sess_num']);
																															unset($_SESSION['sess_Total']);
 
																															
																															exit("<script>alert('บันทึกข้อมูลการสั่งซื้อแล้วค่ะ');window.location='print.php?ID=".$last_id."';</script>");	
																															}else{
																															exit("<script>alert('Error : บันทึกข้อมูลใบสั่งซื้อไม่ได้! ');(history.back());</script>");
																															}
																														 }
																														 
//#########  CANCEL  #######################################//					
}else if($_REQUEST['sql']=="CANCEL"){
	$ordID = $_REQUEST['ID'];
	$status = "6";
	$sql_update = mysqli_query($con,"UPDATE ".$orders." SET "
												." ord_status ='".$status."' WHERE ord_id='".$ordID."'");
												
	if($sql_update){
 
			$ordID = $_REQUEST['ID'];					
			$sql = mysqli_query($con,"SELECT * FROM ".$orders_detail." WHERE od_ord_id='".$ordID."'");				
				while($rst = mysqli_fetch_array($sql)){
						 $od_prdID = $rst['od_prd_id'];
						  $num = $rst['od_num'];
							
										$sql2 = mysqli_query($con,"SELECT * FROM ".$product." WHERE prd_id = '".$od_prdID."'");
										$rst2 = mysqli_fetch_array($sql2);
										$stock = $rst2['prd_stock'];
										$sell = $rst2['prd_sell'];
									 	
								 $up_stock = $stock + $num;
								 $up_sell = $sell - $num;
										
									 $sql_stock =mysqli_query($con,"UPDATE ".$product." SET prd_sell='".$up_sell."', prd_stock='".$up_stock."' WHERE prd_id='".$od_prdID."'");
										}	
								 

			exit("<script>alert('การยกเลิกใบสั่งซื้อสินค้าสำเร็จ');window.location='print.php?ID=".$ordID."';</script>");	
			}else{
			exit("<script>alert('Error : บันทึกข้อมูลไม่ได้! ');(history.back());</script>");
			}
			
//#########  DEL  #######################################//					
}else if($_REQUEST['sql']=="DEL"){ 		
																												 
		$ordID = $_REQUEST['ID'];
		// ลบข้อมูลตามรหัสที่ส่งมา 
		$sql_delete = mysqli_query($con,"DELETE FROM ".$orders."  WHERE ord_id='".$ordID."'");
		
		if($sql_delete){
				$sql_delete = mysqli_query($con,"DELETE FROM ".$orders_detail."  WHERE od_ord_id='".$ordID."'");
				$sql_delete = mysqli_query($con,"DELETE FROM ".$payment."  WHERE pm_ord_id='".$ordID."'");
				
				if($_SESSION['sess_mb_userid'] == session_id()){
					exit("<script>window.location='mb_order.php';</script>");	
					}else{
					exit("<script>window.location='adm_order.php?pp=1';</script>");	
					}
			}else{
			exit("<script>alert('Error : ทำรายการไม่ได้! ');(history.back());</script>");
			}		

//#########  Approve  #######################################//					
}else if($_REQUEST['sql']=="Approve"){ 		

			$ordID = $_REQUEST['ID'];					
			$sql = mysqli_query($con,"SELECT * FROM ".$orders_detail." WHERE od_ord_id='".$ordID."'");				
				while($rst = mysqli_fetch_array($sql)){
						 $od_prdID = $rst['od_prd_id'];
						  $num = $rst['od_num'];
							
										$sql2 = mysqli_query($con,"SELECT * FROM ".$product." WHERE prd_id = '".$od_prdID."'");
										$rst2 = mysqli_fetch_array($sql2);
										$stock = $rst2['prd_stock'];
										$sell = $rst2['prd_sell'];
									 	
							//	 $up_stock = $stock - $num;
								// $up_sell = $sell + $num;
										
									//	$sql_stock =mysqli_query($con,"UPDATE ".$product." SET prd_sell='".$up_sell."', prd_stock='".$up_stock."' WHERE prd_id='".$od_prdID."'");
										}			
																						
			$sql_update = mysqli_query($con,"UPDATE ".$orders." SET ord_status='4' WHERE ord_id='".$ordID."'");
			if($sql_update){
				exit("<script>alert('อนุมัติสินค้าทั้งหมดแล้ว');window.location='print.php?ID=".$ordID."';</script>");	
				}else{
				exit("<script>alert('Error : ทำรายการไม่ได้! ');(history.back());</script>");
				}
			

//#########  SEND  #######################################//					
}else if($_REQUEST['sql']=="SEND"){ 		
	$ordID = $_REQUEST['ID'];	
	$status =5;
	// วันเดือนปี เวลาปัจจุบัน
	$DateTime = date("Y-m-d H:i:s", mktime(date("H")+0, date("i")+0, date("s")+0, date("m")+0 , date("d")+0, date("Y")+0));
	$sql_update = mysqli_query($con,"UPDATE ".$orders." SET "
													." ord_status ='".$status."', ord_date='".$DateTime."' WHERE ord_id='".$ordID."'");
		if($sql_update){
			exit("<script>(history.back());</script>");
			}else{
			exit("<script>alert('Error : ทำรายการไม่ได้! ');(history.back());</script>");
			}	
																	 
}else{
	exit("<script>alert('error : ค้นหา SQL ตาราง ".$TbName." ไม่เจอ!');(history.back());</script>");
	} // End SQL
			
	
//***** ----  เริ่มตารางใหม่ ---- ****************************************************************************************************************** payment //
}else if($TbName=="payment"){ 

	//เวลาและนาที่
		$H = $_POST['txt_H'];
		$I = $_POST['txt_I'];
		
	$date_ary = explode("/", $_POST['txt_date']);
	$day = $date_ary[0];
	$month = $date_ary[1];
	$year = $date_ary[2]-543;
	
	//$txt_date =  $year."-".$month."-".$day; // ทำให้เป็นรูปแบบวันเดือนปีที่ใช้งานได้จริง	
	//รวมวันเวลาเข้าด้วยกัน
	$Date_pay= date("Y-m-d H:i:s", mktime(date("".$H."")+0, date("".$I."")+0, date("s")+0, date("".$month."")+0 , date("".$day."")+0, date("".$year."")+0));
	
	// วันเดือนปี เวลาปัจจุบัน
	$DateTime = date("Y-m-d H:i:s", mktime(date("H")+0, date("i")+0, date("s")+0, date("m")+0 , date("d")+0, date("Y")+0));
	
	
	
	$FileName 	= $_FILES['FileUpload'] ['name'];
		$Filetype 		= $_FILES['FileUpload'] ['type'];
		$FileSize 		= $_FILES['FileUpload'] ['size'];
		$FileUpLoadtmp = $_FILES['FileUpload'] ['tmp_name'];
				
	if($FileUpLoadtmp){					 
		$array_last = explode(".",$FileName); // เป็น array หาจำนวน จุด . ของชื่อตัวแปร์		
		$c = count($array_last) - 1; //นับจำนวน จุด "." ของชื่อตัวแปร์ 
		$lname = strtolower($array_last [$c]); // หา นามสกุลไฟล์ ตัวสุดท้ายของ ตัวแปร์
		$NewFileupload = date("U"); 
		$NewFile = $NewFileupload.".$lname"; //รวม ชื่อและนามสกลุดไฟล์เข้าด้วยกัน 
		}
	

 //#########  payment #######################################//
	if($_REQUEST['sql']=="ADD"){  
		$status = "1"; //สถานะ
		//เพิ่มข้อมูลลงในตารางโดยส่งค่ามาจาก ฟอร์ม
		$sql_insert = mysqli_query($con,"INSERT INTO ".$payment." VALUES "
				."(0,
				'".$_POST['txt_ord_id']."',
				'".$_POST['txt_bank']."', 
				'".$_POST['txt_price']."', 
				'".$NewFile."', 
				'".$Date_pay."', 
				'".$DateTime."', 
				'".$status."')");
				
		if($sql_insert){
		$last_id = mysqli_insert_id($con);
				if($lname=="gif" or $lname=="jpg" or $lname=="jpeg" or $lname=="png"){
					//Upload File รูปภาพลงในโฟลเดอร์  photo
					$UploadFile = move_uploaded_file($FileUpLoadtmp, "photo/".$NewFile);					
					}	
		$ID = $_REQUEST['txt_ord_id'];
		$status = "2";
		$sql_update = mysqli_query($con,"UPDATE ".$orders." SET "
																	 			  ." ord_status ='".$status."' WHERE ord_id='".$ID."'");
									
			exit("<script>alert('บันทึกข้อมูลการแจ้งชำระเงินแล้ว');window.location='print.php?ID=".$ID."';</script>");	
			}else{
			exit("<script>alert('Error : บันทึกข้อมูลไม่ได้! ');(history.back());</script>");
			}

 //#########  payment #######################################//
	}else if($_REQUEST['sql']=="UPDATE"){ 
	// วันเดือนปี เวลาปัจจุบัน
	$DateTime = date("Y-m-d H:i:s", mktime(date("H")+0, date("i")+0, date("s")+0, date("m")+0 , date("d")+0, date("Y")+0));
	$status = "2";
	$sql_update = mysqli_query($con,"UPDATE ".$payment." SET "." 
															pm_status ='".$status."' WHERE pm_id='".$_GET['ID']."'");

	if($sql_update){
		$sql = mysqli_query($con,"SELECT * FROM ".$payment." WHERE pm_id = '".$_GET['ID']."'");
		$rst = mysqli_fetch_array($sql);
		$ordID = $rst['pm_ord_id'];

	$status = "3";
	$sql_update = mysqli_query($con,"UPDATE ".$orders." SET "
												." ord_status ='".$status."' WHERE ord_id='".$ordID."'");
												
			exit("<script>(history.back());</script>");
			}else{
			exit("<script>alert('Error : ทำรายการไม่ได้! ');(history.back());</script>");
			}	
	
	
 //#########  payment #######################################//
	}else if($_REQUEST['sql']=="DEL"){ 
			// ลบข้อมูลตามรหัสที่ส่งมา 
		$Del = mysqli_query($con,"DELETE FROM ".$payment."  WHERE pm_id='".$_GET['ID']."'");
		
		if($Del){
			exit("<script>(history.back());</script>");
			}else{
			exit("<script>alert('Error : ทำรายการไม่ได้! ');(history.back());</script>");
			}	
				
	}else{
		exit("<script>alert('error : ค้นหา SQL ตาราง ".$TbName." ไม่เจอ!');(history.back());</script>");
		} // End SQL
		

//***** ----  เริ่มตารางใหม่ ---- ****************************************************************************************************************** board_question //		
}else if($TbName=="board_question"){ 	
	  //#########  board_question #######################################//
	 if($_REQUEST['sql']=="ADD"){ 		
		$Detail = $_POST['txt_detail'];
		if(empty($Detail)){
			exit("<script>alert('กรุณากรอกข้อมูลบทความเว็บบอร์ดด้วยนะ!');(history.back());</script>");
			}else{
		
			// วันเดือนปี เวลาปัจจุบัน
			$DateTime = date("Y-m-d H:i:s", mktime(date("H")+0, date("i")+0, date("s")+0, date("m")+0 , date("d")+0, date("Y")+0));
			$txtDetail = eregi_replace(chr(13),"<br>",$_POST['txt_detail']);
			$sql_insert = mysqli_query($con,"INSERT INTO ".$board_question."
																										 VALUES
																											  ('0',
																												'".$_POST['txt_name']."',
																												'".$_POST['txt_title']."',
																												'".$_POST['txt_email']."',
																												'0',
																												'".$_POST['radiobutton']."',
																												'".$txtDetail."',
																												'".$DateTime."')");
				
				if($sql_insert){
				exit("<script>alert('บันทึกข้อมูลแล้ว');window.location='board.php?pp=1';</script>");	
				}else{
				exit("<script>alert('Error : บันทึกข้อมูลไม่ได้! ');(history.back());</script>");
				}
			}
			
	 //#########  board_question #######################################//	
	}else if($_REQUEST['sql']=="UPDATE"){ 
	
		$ID = $_REQUEST['ID'];			
		$sql = mysqli_query($con,"SELECT * FROM ".$board_question." WHERE topic_id='".$ID."'");
			  $rst = mysqli_fetch_array($sql);
							$num = $rst['topic_num']+1;
								
			  $sql_update = mysqli_query($con,"UPDATE ".$board_question." SET topic_num='".$num."' WHERE topic_id='".$ID."'");
				if($sql_update){
					exit("<script>window.location='title_board.php?pp=1&ID=".$ID."';</script>");	
					}else{
					exit("<script>alert('Error : Update จำนวนผู้เข้าชมไม่ได้ ! ');(history.back());</script>");
					}
					
		 //#########  board_question #######################################//	
		}else if($_REQUEST['sql']=="DEL"){ 

				if(!$_REQUEST['qst_id']==""){
						$sql_del1 =mysqli_query($con,"DELETE FROM ".$board_question."  WHERE topic_id='".$_REQUEST['qst_id']."'");
						$sql_del2 =mysqli_query($con,"DELETE FROM ".$board_answer."  WHERE ans_IDtopic='".$_REQUEST['qst_id']."'");
						
					}else if(!$_REQUEST['ans_id']==""){
						$sql_del3 = mysqli_query($con,"DELETE FROM ".$board_answer."  WHERE ans_id='".$_REQUEST['ans_id']."'");
						}
					
					if(isset($_REQUEST['ans_id'])){
							exit("<script>window.location='title_board.php?pp=1&ID=".$_REQUEST['qstID']."';</script>");	
							}else{
							exit("<script>window.location='board.php?pp=1';</script>");	
							}
		
	 }else{
	 exit("<script>alert('error : ค้นหา SQL ตาราง ".$TbName." ไม่เจอ!');(history.back());</script>");
	 }

		
		
//***** ----  เริ่มตารางใหม่ ---- ****************************************************************************************************************** board_answer //
}else if($TbName=="board_answer"){ 	

	  //#########  board_answer #######################################//
	 if($_REQUEST['sql']=="ADD"){ 		
		$Detail = $_POST['txt_detail'];
		if(empty($Detail)){
			exit("<script>alert('กรุณากรอกข้อมูลบทความเว็บบอร์ดด้วยนะ!');(history.back());</script>");
			}else{
		
			$ID = $_REQUEST['ID'];
			// วันเดือนปี เวลาปัจจุบัน
			$DateTime = date("Y-m-d H:i:s", mktime(date("H")+0, date("i")+0, date("s")+0, date("m")+0 , date("d")+0, date("Y")+0));
			$txtDetail = eregi_replace(chr(13),"<br>",$_POST['txt_detail']);
			$sql_insert = mysqli_query($con,"INSERT INTO ".$board_answer." 
																									VALUES('0',
																									'".$ID."',
																									'".$_POST['txt_name']."',
																									'".$_POST['txt_email']."',
																									'".$txtDetail."',
																									'".$DateTime."')");
				
				if($sql_insert){
				exit("<script>alert('บันทึกข้อมูลแล้ว');window.location='title_board.php?pp=1&ID=".$ID."#".$ID."';</script>");	
				}else{
				exit("<script>alert('Error : บันทึกข้อมูลไม่ได้! ');(history.back());</script>");
				}
			}
		}
		
//######### -----------------จบ---ชื่อตารางไม่ตรงกับที่กำหนดไว้-----------------  #######################################//		
}else{
	exit("<script>alert('ชื่อตารางไม่ตรงกับที่กำหนดไว้! ');(history.back());</script>");
	}
	
//######### -----------------จบ--------------------  #######################################//	
}else{
exit("<script>alert('ไม่พบ ชื่อตาราง ที่ส่งมาด้วย!');(history.back());</script>");
}	
	
//######### -- สคริปใช้บ่อย --- #########//
$TxtMsG = "สคริปใช้บ่อย";
if(empty($TxtMsG)){
	exit("<script>alert(' ข้อความ !');(history.back());</script>");
	exit("<script>alert('ข้อความ');window.location='PageLink.php';</script>");	
	exit("<script>window.location='PageLink.php';</script>");	
	}
//######### -- สคริปใช้บ่อย --- #########//
?>



























 
</body>
</html>
