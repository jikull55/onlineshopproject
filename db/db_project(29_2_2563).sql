-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 29, 2020 at 02:56 AM
-- Server version: 5.7.17-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `adm_id` int(10) NOT NULL COMMENT 'รหัสตาราง(pk)',
  `adm_user` varchar(20) NOT NULL COMMENT 'ใช้ Login เข้าระบบ',
  `adm_pass` varchar(15) NOT NULL COMMENT 'รหัสผ่าน',
  `adm_name` varchar(40) NOT NULL COMMENT 'ชื่อ - นามสกุล',
  `adm_birthday` date NOT NULL COMMENT 'วันเกิด',
  `adm_address` text NOT NULL COMMENT 'ที่อยู่',
  `adm_tel` varchar(10) NOT NULL COMMENT 'เบอร์โทร',
  `adm_email` varchar(150) NOT NULL COMMENT 'อีเมล์',
  `adm_status` varchar(20) NOT NULL COMMENT 'สถานะสิทธิ์การใช้ระบบ',
  `adm_date` datetime NOT NULL COMMENT 'วันที่'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='ตารางผู้ดูแลระบบ';

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`adm_id`, `adm_user`, `adm_pass`, `adm_name`, `adm_birthday`, `adm_address`, `adm_tel`, `adm_email`, `adm_status`, `adm_date`) VALUES
(13, 'admin', '1234', 'admin', '2530-07-12', '55/263 ประเสริฐมนูกิจ42-คลองกุ่ม-บึงกุ่ม-กรุงเทพมหานคร-10230', '0948645250', 'jikull55@gmail.com', 'ผู้ดูแลระบบ', '2013-07-25 02:50:03');

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `bn_id` int(11) NOT NULL COMMENT 'รหัสตาราง(pk)',
  `bn_bank` varchar(150) NOT NULL COMMENT 'ชื่อธนาคาร',
  `bn_name` varchar(150) NOT NULL COMMENT 'ชื่อบัญชี',
  `bn_number` varchar(150) NOT NULL COMMENT 'เลขที่บัญชี',
  `bn_branch` varchar(150) NOT NULL COMMENT 'สาขา',
  `bn_photo` varchar(25) NOT NULL COMMENT 'รูป'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='ตารางธนาคาร';

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`bn_id`, `bn_bank`, `bn_name`, `bn_number`, `bn_branch`, `bn_photo`) VALUES
(11, 'กรุงไทย', 'xxxxxx', '1111111111', 'กรุงเทพ', '1375497743.png'),
(13, 'กสิกรไทย1', 'สุริยา จันทะนาลา', '4480045033', 'ร้อยเอ็ด', '1375497751.png');

-- --------------------------------------------------------

--
-- Table structure for table `board_answer`
--

CREATE TABLE `board_answer` (
  `ans_id` int(11) NOT NULL,
  `ans_IDtopic` int(11) NOT NULL,
  `ans_name` varchar(50) NOT NULL,
  `ans_email` varchar(100) NOT NULL,
  `ans_detail` text NOT NULL,
  `ans_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `board_question`
--

CREATE TABLE `board_question` (
  `topic_id` int(11) NOT NULL,
  `topic_name` varchar(50) NOT NULL,
  `topic_title` varchar(150) NOT NULL,
  `topic_email` varchar(100) NOT NULL,
  `topic_num` int(11) NOT NULL,
  `topic_photo` varchar(20) NOT NULL,
  `topic_detail` text NOT NULL,
  `topic_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `board_question`
--

INSERT INTO `board_question` (`topic_id`, `topic_name`, `topic_title`, `topic_email`, `topic_num`, `topic_photo`, `topic_detail`, `topic_date`) VALUES
(6, 'จิรายุ กัลปพงศ์', 'ขี้แดดนาเกลือคืออะไร', 'jikull55@gmail.com', 2, '001.png', 'ขี้แดดนาเกลือคืออะไร', '2020-02-12 07:12:22');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `mb_id` int(10) NOT NULL COMMENT 'รหัสสมาชิก(pk)',
  `mb_user` varchar(20) NOT NULL COMMENT 'ใช้ Login เข้าระบบ',
  `mb_pass` varchar(15) NOT NULL COMMENT 'รหัสผ่าน',
  `mb_name` varchar(40) NOT NULL COMMENT 'ชื่อ - นามสกุล',
  `mb_birthday` date NOT NULL COMMENT 'วันเกิด',
  `mb_address` text NOT NULL COMMENT 'ที่อยู่',
  `mb_tel` varchar(10) NOT NULL COMMENT 'เบอร์โทร',
  `mb_email` varchar(150) NOT NULL COMMENT 'อีเมล์',
  `mb_status` varchar(20) NOT NULL COMMENT 'สถานะสิทธิ์การใช้ระบบ',
  `mb_date` datetime NOT NULL COMMENT 'วันที่สมัครสมาชิก'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='ตารางสมาชิก';

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`mb_id`, `mb_user`, `mb_pass`, `mb_name`, `mb_birthday`, `mb_address`, `mb_tel`, `mb_email`, `mb_status`, `mb_date`) VALUES
(5, 'user', '1234', 'จิรายุ กัลปพงศ์', '2541-03-01', '55/263 ประเสริฐมนูกิจ42-คลองกุ่ม-บึงกุ่ม-กรุงเทพมหานคร-10230', '0948645250', 'jikull55@gmail.com', '', '2013-07-30 15:45:05');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `ord_id` int(11) NOT NULL COMMENT 'รหัสตาราง(pk)',
  `ord_mb_id` int(11) NOT NULL COMMENT 'รหัสสมาชิก(fk)',
  `ord_name` varchar(150) NOT NULL COMMENT 'ชื่อสมาชิก',
  `ord_tel` varchar(15) NOT NULL COMMENT 'เบอร์โทร',
  `ord_total` float NOT NULL COMMENT 'ราคารวม',
  `ord_date` datetime NOT NULL COMMENT 'วันที่สั่งซื้อ',
  `ord_status` varchar(5) NOT NULL COMMENT 'สถานะใบสั่งซื้อ'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='ตารางใบสั่งซื้อ';

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`ord_id`, `ord_mb_id`, `ord_name`, `ord_tel`, `ord_total`, `ord_date`, `ord_status`) VALUES
(3, 5, 'จิรายุ กัลปพงศ์', '0948645250', 2500, '2020-02-12 06:57:16', '5');

-- --------------------------------------------------------

--
-- Table structure for table `orders_detail`
--

CREATE TABLE `orders_detail` (
  `od_ord_id` int(11) NOT NULL COMMENT 'รหัสใบสั่งซื้อ(fk)',
  `od_prd_id` int(11) NOT NULL COMMENT 'รหัสสินค้า(fk)',
  `od_num` int(11) NOT NULL COMMENT 'จำนวนสินค้าที่สั่งซื้อ',
  `od_price` float NOT NULL COMMENT 'ราคา'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='ตารางรายละเอียดใบสั่งซื้อ';

--
-- Dumping data for table `orders_detail`
--

INSERT INTO `orders_detail` (`od_ord_id`, `od_prd_id`, `od_num`, `od_price`) VALUES
(3, 1, 5, 500);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `pm_id` int(10) NOT NULL COMMENT 'รหัสตาราง(pk)',
  `pm_ord_id` int(10) NOT NULL COMMENT 'รหัสใบจอง(fk)',
  `pm_bank_id` int(10) NOT NULL COMMENT 'รหัสธนาคาร(fk)',
  `pm_price` double NOT NULL COMMENT 'ราคา',
  `photo` varchar(150) NOT NULL COMMENT 'รูปภาพ',
  `pm_pay` datetime NOT NULL COMMENT 'วันที่ชำระเงินจริง',
  `pm_date` datetime NOT NULL COMMENT 'วันที่แจ้งชำระเงิน',
  `pm_status` varchar(20) NOT NULL COMMENT 'สถานะ'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='ตารางแจ้งชำระเงิน';

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`pm_id`, `pm_ord_id`, `pm_bank_id`, `pm_price`, `photo`, `pm_pay`, `pm_date`, `pm_status`) VALUES
(3, 3, 11, 2500, '', '1998-03-01 03:15:43', '2020-02-12 06:53:43', '2');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `prd_id` int(10) NOT NULL COMMENT 'รหัสสินค้า(pk)',
  `prd_type_id` int(10) NOT NULL COMMENT 'รหัสประเภทสินค้า(fk)',
  `prd_name` varchar(100) NOT NULL COMMENT 'ชื่อสินค้า',
  `prd_price` float NOT NULL COMMENT 'ราคาสินค้า',
  `prd_size` varchar(50) NOT NULL COMMENT 'ขนาดสินค้า',
  `prd_detail` text NOT NULL COMMENT 'รายละเอียดสินค้า',
  `prd_stock` int(10) NOT NULL COMMENT 'จำนวนสินค้า',
  `prd_sell` int(10) NOT NULL COMMENT 'จำนวนสินค้าที่ขายไป',
  `prd_photo` varchar(30) NOT NULL COMMENT 'รูปสินค้า',
  `prd_status` varchar(30) NOT NULL COMMENT 'สถานะ'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='ตารางสินค้า';

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`prd_id`, `prd_type_id`, `prd_name`, `prd_price`, `prd_size`, `prd_detail`, `prd_stock`, `prd_sell`, `prd_photo`, `prd_status`) VALUES
(1, 1, 'สารปรับปรุงดินขี้แดดนาเกลือ', 500, 'xxx', 'asdada  กหฟกฟหกฟหก <br>\n', 95, 5, '1581389427.jpg', 'สินค้าแนะนำ'),
(8, 1, 'สารปรับปรุงดินขี้แดดนาเกลือ2', 600, 'ปป', 'ปปป', 2, 0, '1581491188.jpg', 'สินค้าปกติ');

-- --------------------------------------------------------

--
-- Table structure for table `product_type`
--

CREATE TABLE `product_type` (
  `type_id` int(10) NOT NULL COMMENT 'รหัสประเภทสินค้า(pk)',
  `type_name` varchar(100) NOT NULL COMMENT 'ชื่อประเภทสินค้า'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='ตารางประเภทสินค้า';

--
-- Dumping data for table `product_type`
--

INSERT INTO `product_type` (`type_id`, `type_name`) VALUES
(1, 'สินค้า');

-- --------------------------------------------------------

--
-- Table structure for table `province`
--

CREATE TABLE `province` (
  `province_id` int(2) NOT NULL COMMENT 'รหัสตาราง(pk)',
  `province_name` varchar(50) NOT NULL COMMENT 'ชื่อจังหวัด'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='ตารางข้อมูลจังหวัดในประเทศไทย';

--
-- Dumping data for table `province`
--

INSERT INTO `province` (`province_id`, `province_name`) VALUES
(1, 'กระบี่'),
(2, 'กรุงเทพมหานคร'),
(3, 'กาญจนบุรี'),
(4, 'กาฬสินธุ์'),
(5, 'กำแพงเพชร'),
(6, 'ขอนแก่น'),
(7, 'จันทบุรี'),
(8, 'ฉะเชิงเทรา'),
(9, 'ชลบุรี'),
(10, 'ชัยนาท'),
(11, 'ชัยภูมิ'),
(12, 'ชุมพร'),
(13, 'เชียงราย'),
(14, 'เชียงใหม่'),
(15, 'ตรัง'),
(16, 'ตราด'),
(17, 'ตาก'),
(18, 'นครนายก'),
(19, 'นครปฐม'),
(20, 'นครพนม'),
(21, 'นครราชสีมา'),
(22, 'นครศรีธรรมราช'),
(23, 'นครสวรรค์'),
(24, 'นนทบุรี'),
(25, 'นราธิวาส'),
(26, 'น่าน'),
(27, 'บุรีรัมย์'),
(28, 'ปทุมธานี'),
(29, 'ประจวบคีรีขันธ์'),
(30, 'ปราจีนบุรี'),
(31, 'ปัตตานี'),
(32, 'พระนครศรีอยุธยา'),
(33, 'พะเยา'),
(34, 'พังงา'),
(35, 'พัทลุง'),
(36, 'พิจิตร'),
(37, 'พิษณุโลก'),
(38, 'เพชรบุรี'),
(39, ' เพชรบูรณ์'),
(40, 'แพร่'),
(41, 'ภูเก็ต'),
(42, 'มหาสารคาม'),
(43, 'มุกดาหาร'),
(44, 'แม่ฮ่องสอน'),
(45, 'ยโสธร'),
(46, 'ยะลา'),
(47, 'ร้อยเอ็ด'),
(48, 'ระนอง'),
(49, 'ระยอง'),
(50, 'ราชบุรี'),
(51, 'ลพบุรี'),
(52, 'ลำปาง'),
(53, 'ลำพูน'),
(54, 'เลย'),
(55, 'ศรีสะเกษ'),
(56, 'สกลนคร'),
(57, 'สงขลา'),
(58, 'สตูล'),
(59, 'สมุทรปราการ'),
(60, 'สมุทรสงคราม'),
(61, 'สมุทรสาคร'),
(62, 'สระแก้ว'),
(63, 'สระบุรี'),
(64, 'สิงห์บุรี'),
(65, 'สุโขทัย'),
(66, 'สุพรรณบุรี'),
(67, 'สุราษฎร์ธานี'),
(68, 'สุรินทร์'),
(69, 'หนองคาย'),
(70, 'หนองบัวลำภู'),
(71, 'อ่างทอง'),
(72, 'อำนาจเจริญ'),
(73, 'อุดรธานี'),
(74, 'อุตรดิตถ์'),
(75, 'อุทัยธานี'),
(76, 'อุบลราชธานี'),
(77, 'จังหวัดบึงกาฬ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`adm_id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`bn_id`);

--
-- Indexes for table `board_answer`
--
ALTER TABLE `board_answer`
  ADD PRIMARY KEY (`ans_id`);

--
-- Indexes for table `board_question`
--
ALTER TABLE `board_question`
  ADD PRIMARY KEY (`topic_id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`mb_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`ord_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`pm_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`prd_id`);

--
-- Indexes for table `product_type`
--
ALTER TABLE `product_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`province_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `adm_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสตาราง(pk)', AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `bn_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสตาราง(pk)', AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `board_answer`
--
ALTER TABLE `board_answer`
  MODIFY `ans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `board_question`
--
ALTER TABLE `board_question`
  MODIFY `topic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `mb_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสสมาชิก(pk)', AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `ord_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสตาราง(pk)', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `pm_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสตาราง(pk)', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `prd_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสสินค้า(pk)', AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `product_type`
--
ALTER TABLE `product_type`
  MODIFY `type_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสประเภทสินค้า(pk)', AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `province`
--
ALTER TABLE `province`
  MODIFY `province_id` int(2) NOT NULL AUTO_INCREMENT COMMENT 'รหัสตาราง(pk)', AUTO_INCREMENT=78;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
