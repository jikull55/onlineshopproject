<?php
 session_start(); 
 include "inc_TitlePage.php";
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script type="text/javascript" src="images/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="slimbox/js/slimbox2.js"></script>
<link rel="stylesheet" href="slimbox/css/slimbox2.css" type="text/css" media="screen" />

<link rel="stylesheet" type="text/css" href="css_style_index.css" />
<link rel="stylesheet" type="text/css" href="css_style_menu.css" />

		<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />	
		<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.10.offset.datepicker.min.js"></script>
		<script type="text/javascript">
		  $(function () {
		    var d = new Date();
		    var toDay = d.getDate() + '/' + (d.getMonth() + 1) + '/' + (d.getFullYear() + 543);


		    // กรณีต้องการใส่ปฏิทินลงไปมากกว่า 1 อันต่อหน้า ก็ให้มาเพิ่ม Code ที่บรรทัดด้านล่างด้วยครับ (1 ชุด =  1 ปฏิทิน)

		    $("#datepicker-now").datepicker({ changeMonth: true, changeYear: true,dateFormat: 'dd/mm/yy', isBuddhist: true, defaultDate: toDay, dayNames: ['อาทิตย์','จันทร์','อังคาร','พุธ','พฤหัสบดี','ศุกร์','เสาร์'],
              monthNames: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],
              monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.']});
			  

     		    $("#datepicker-en").datepicker({ dateFormat: 'dd/mm/yy'});

		    $("#inline").datepicker({ dateFormat: 'dd/mm/yy', inline: true });


			});
		</script>
		<style type="text/css">

			.demoHeaders { margin-top: 2em; }
			#dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
			#dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
			ul#icons {margin: 0; padding: 0;}
			ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
			ul#icons span.ui-icon {float: left; margin: 0 4px;}
			ul.test {list-style:none; line-height:30px;}
		
			
label{
width: 100px; 
display: inline-block;
}
.ui-datepicker{  
    width:200px;  
    font-family:tahoma;  
    font-size:13px;  
}  
</style>
<title><?php echo $TitlePage; ?></title>
</head>
<body id="Page0">
<div class="head">
<?php include "inc_header.php"; ?>
</div>
<div>
<table border="0" align="center" cellpadding="0" cellspacing="0" class="table_main">


  <tr>
    <td align="left" valign="top" class="table_menu_left" id="">
	<?php include "inc_menu_left.php"; ?>	</td>
    <td width="750" align="left" valign="top" class="table_body_center">
	<div class="title">
		  <h2> <img src="images/emp_32list.png" border="0" /> สมัครสมาชิกใหม่ </h2>
	</div>
	<div class="box">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<form action="actionSQL.php" method="post"  id="form1"  name="form1" enctype="multipart/form-data" onsubmit="return check_text();">
	<?php include "inc_chk_txt_from.php"; ?>
			
					<tr>
                        <td width="18%" height="27" align="right" valign="middle"><h5>Username  :</h5></td>
                        <td width="82%" height="30" align="left" valign="middle">
						<input  class="frm" name="txt_user" type="text" id="txt" value="<?php echo $_SESSION['txt_user']; ?>"  style="width: 200px;" /></td>
   		    </tr>
				  <tr>
                    <td width="18%" height="27" align="right" valign="middle"><h5>Password  :</h5></td>
                        <td width="82%" height="30" align="left" valign="middle">
					<input  class="frm" name="txt_pass" type="password" id="txt"  style="width: 200px;" /></td>
          		  </tr>
					  <tr>
                        <td width="18%" height="27" align="right" valign="middle"><h5>ชื่อ-สกุล  :</h5></td>
                        <td width="82%" height="30" align="left" valign="middle">
						<input  class="frm" name="txt_name" type="text" id="txt" value="<?php echo $_SESSION['txt_name']; ?>"  style="width: 200px;" /></td>
                      </tr>
					  <tr>
						<td height="25" align="right" valign="middle"><h5>วันเกิด : </h5></td>
						<td align="left" valign="middle">
							<input name="txt_date"  type="text" class="frm" id="datepicker-now" style="width: 100px; color:#000;" value="<?php echo $_SESSION['txt_date']; ?>" /></td>
					  </tr>
					  
					  <tr>
                        <td width="18%" height="27" align="right" valign="middle"><h5>ที่อยู่  :</h5></td>
                        <td width="82%" height="30" align="left" valign="middle">
						<input class="frm" name="txt_ads[]" type="text" id="txt"  style="width: 300px;" value="<?php echo $_SESSION['txt_ads1']; ?>" /></td>
                      </tr>
					  
					  <tr>
                        <td width="18%" height="27" align="right" valign="middle"><h5>แขวง/ตำบล  :</h5></td>
                        <td width="82%" height="30" align="left" valign="middle">
						<input class="frm" name="txt_ads[]" type="text" id="txt"  style="width: 150px;" value="<?php echo $_SESSION['txt_ads2']; ?>" maxlength="20" /></td>
                      </tr>
					  
					  <tr>
                        <td width="18%" height="27" align="right" valign="middle"><h5>เขต/อำเภอ    :</h5></td>
                        <td width="82%" height="30" align="left" valign="middle">
						<input name="txt_ads[]" type="text" class="frm" id="txt"  style="width: 150px;" value="<?php echo $_SESSION['txt_ads3']; ?>" maxlength="20" /></td>
                      </tr>
					  <tr>
                        <td width="18%" height="27" align="right" valign="middle"><h5>จังหวัด  :</h5></td>
                        <td width="82%" height="30" align="left" valign="middle">
						<select class="frm" name="txt_ads[]" id="txt" style="width: 30%;">
        <option value="" selected="selected">----เลือกข้อมูล----</option>
        	<?php
						include "connect_db.php";
							$sql_select2 = mysqli_query($con,"SELECT * FROM ".$province."");
							
							while($rs=mysqli_fetch_array($sql_select2)) {
									$id2 =$rs['province_id'];
									$name2 =$rs['province_name'];
									
								if($_SESSION['txt_ads4']==$name2){
										echo  "<option value =".$name2." selected>".$name2."</option>";
										}else{
										echo  "<option value = ".$name2.">".$name2."</option>";
										}					
							}
						?>
      </select></td>
                      </tr>
					  
					  <tr>
                        <td width="18%" height="27" align="right" valign="middle"><h5> รหัสไปรษณีย์   :</h5></td>
                        <td width="82%" height="30" align="left" valign="middle">
						<input class="frm" name="txt_ads[]" type="text" id="txt"  style="width: 100px;" value="<?php echo $_SESSION['txt_ads5']; ?>" maxlength="5" /></td>
                      </tr>
					  
					  <tr>
                        <td width="18%" height="27" align="right" valign="middle"><h5>เบอร์โทร  :</h5></td>
                        <td width="82%" height="30" align="left" valign="middle">
						<input class="frm" name="txt_tel" type="text" id="txt"  style="width: 200px;" value="<?php echo $_SESSION['txt_tel']; ?>" maxlength="10" /></td>
                      </tr>
					  
					  <tr>
                        <td width="18%" height="27" align="right" valign="middle"><h5>อีเมล์  :</h5></td>
                        <td width="82%" height="30" align="left" valign="middle">
						<input class="frm" name="txt_email" type="text" id="txt"  style="width: 200px;" value="<?php echo $_SESSION['txt_email']; ?>" /></td>
                      </tr>
					  
                      <tr>
                        <td height="30" align="right" valign="middle">&nbsp;</td>
                        <td height="30" align="left" valign="middle">
						<input class="button_txt"  type="submit" name="confirm" id="confirm" value="บันทึกข้อมูล" />
              		  	<input class="button_txt"  type="button" name="button"  id="b" value="ย้อนกลับ" onclick="(history.back())" />
                        <input type="hidden" name="TbName" value="member" />
						<input type="hidden" name="sql" value="ADD" />						</td>
                      </tr>
	      </form>
      </table>
    </div>
	<p>&nbsp;</p>
    <p>&nbsp;</p>
    </td>
  </tr>
</table>
<div class="footer">
<?php include "inc_footer.php"; ?>
</div>
</div>
</body>
</html>